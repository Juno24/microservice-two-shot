# Wardrobify

Team:

* Juno - Shoes
* Gavin - Hats

## Design
This sounds like a weekend problem (Didn't have enough time to master react and apply...) BUT trust us! We shall nail this down eventually ;)

## Shoes microservice

BinVO model is the value object for Shoes microservice to interact with Wardrobe project, which is why BinVo is the aggregate of Shoes model. The integration script is the pollar.py, which is grabbing the Bin's properties from Wardrobe and updating BinVO model, which grants access to create, add, edit and remove.


## Hats microservice

The hat model has 5 fields on it incliding a location forigen key that relates to the LocationVO model. The LocationVO is what we use to connect the wardobe locations to the hats microservice. This allows us to use polling to update our database so we can create, read, update, and delete hats.
