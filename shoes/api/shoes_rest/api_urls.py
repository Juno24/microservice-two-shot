from django.urls import path
from .api_views import api_list_shoes, api_show_shoes

urlpatterns = [
    path("shoes/<int:pk>/", api_show_shoes, name="show"),
    path("shoes/", api_list_shoes, name = "list"),
]
