# Generated by Django 4.0.3 on 2023-04-20 14:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='binvo',
            name='bin_number',
        ),
        migrations.AddField(
            model_name='binvo',
            name='closet_name',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
