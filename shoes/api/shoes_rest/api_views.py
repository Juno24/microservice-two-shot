from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Shoes, BinVO
from common.json import ModelEncoder

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "model_name",
        "color",
        "picture",
        ]

class BinVoEncoder(ModelEncoder):
    model =  BinVO
    properties = [
        "import_href",
        "closet_name",
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
    ]
    encoders = {
        "bin": BinVoEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"] #?
            bin = BinVO.objects.get(import_href=bin_href) #?
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):
    shoe = get_object_or_404(Shoes, id=pk)
    if request.method == "GET":
        try:
            shoe = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoes.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoes.objects.get(id=pk)

            properties = ["manufacturer","model_name","color","picture","bin",]
            for propertie in properties:
                if propertie in properties:
                    setattr(shoe, propertie, content[propertie])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
